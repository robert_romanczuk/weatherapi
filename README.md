### How do I get set up? ###

### In app root directory ###
1. gradlew.bat clean build or ./gradlew clean build
1. gradlew.bat bootRun or ./gradlew bootRun

### Example requests ###
http://localhost:8080/city-weather/Beijing

http://localhost:8080/city-weather/London