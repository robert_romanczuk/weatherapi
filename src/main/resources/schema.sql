CREATE TABLE IF NOT EXISTS Cities (
  id      INTEGER UNIQUE,
  name    VARCHAR(50),
  lon     FLOAT,
  lat     FLOAT,
  country VARCHAR(25)
);

CREATE TABLE IF NOT EXISTS Weather (
  id          INTEGER PRIMARY KEY,
  city_id     INTEGER UNIQUE,
  visibility  INTEGER,
  temp        INTEGER,
  pressure    INTEGER,
  humidity    INTEGER,
  temp_min    FLOAT,
  temp_max    FLOAT,
  wind_speed  FLOAT,
  wind_deg    INTEGER,
  clouds_all  INTEGER,
  sys_type    INTEGER,
  sys_id      INTEGER,
  sys_message INTEGER,
  sys_sunrise INTEGER,
  sys_sunset  INTEGER,
  dt          TIME,
  updated_at DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (city_id) REFERENCES Cities (id)
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_weather_city_id
  ON Weather (city_id);

CREATE TABLE IF NOT EXISTS WeatherDescription (
  id          INTEGER PRIMARY KEY,
  city_id     INTEGER,
  description VARCHAR(200),
  icon        VARCHAR(10),
  wd_id       INTEGER,
  main        VARCHAR(10),
  FOREIGN KEY (city_id) REFERENCES Cities (id)
);