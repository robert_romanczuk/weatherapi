package com.service;

import com.model.WeatherJsonModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SqliteService {

    public JdbcTemplate jdbcTemplate;

    @Autowired
    public SqliteService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void saveCityWeather(String cityName, WeatherJsonModel weatherJsonModel) {
        saveCity(cityName, weatherJsonModel);
        saveWeatherDescription(weatherJsonModel);

        jdbcTemplate.update("REPLACE INTO Weather (city_id, visibility, temp, pressure, humidity, " +
                        "temp_min, temp_max, wind_speed, wind_deg, clouds_all, sys_type, sys_id, sys_message, sys_sunrise, sys_sunset, dt)" +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                weatherJsonModel.getId(),
                weatherJsonModel.getVisibility(),
                weatherJsonModel.getMain().getTemp(),
                weatherJsonModel.getMain().getPressure(),
                weatherJsonModel.getMain().getHumidity(),
                weatherJsonModel.getMain().getTemp_min(),
                weatherJsonModel.getMain().getTemp_max(),
                weatherJsonModel.getWind().getSpeed(),
                weatherJsonModel.getWind().getDeg(),
                weatherJsonModel.getClouds().getAll(),
                weatherJsonModel.getSys().getType(),
                weatherJsonModel.getSys().getId(),
                weatherJsonModel.getSys().getMessage(),
                weatherJsonModel.getSys().getSunrise(),
                weatherJsonModel.getSys().getSunset(),
                weatherJsonModel.getDt()
        );
    }

    private void saveCity(String cityName, WeatherJsonModel weatherJsonModel) {
        jdbcTemplate.update("REPLACE INTO Cities (id, name, lon, lat, country) VALUES(?, ?, ?, ?, ?)",
                weatherJsonModel.getId(),
                cityName.toLowerCase(),
                weatherJsonModel.getCoord().getLon(),
                weatherJsonModel.getCoord().getLat(),
                weatherJsonModel.getSys().getCountry()
        );
    }

    private void saveWeatherDescription(WeatherJsonModel weatherModel) {

        jdbcTemplate.update("DELETE FROM WeatherDescription WHERE city_id = ?", weatherModel.getId());

        for (WeatherJsonModel.Weather w : weatherModel.getWeather()) {
            jdbcTemplate.update("REPLACE INTO WeatherDescription (city_id, description, icon, wd_id, main) VALUES(?, ?, ?, ?, ?)",
                    weatherModel.getId(),
                    w.getDescription(),
                    w.getIcon(),
                    w.getId(),
                    w.getMain()
            );
        }
    }

    public List<Map<String, Object>> getCityWeather(String cityName) {
        return jdbcTemplate.queryForList(
                "SELECT * FROM Weather w LEFT JOIN Cities c ON w.city_id = c.id LEFT JOIN WeatherDescription wd ON wd.city_id = c.id WHERE c.name = ?",
                cityName.toLowerCase()
        );
    }
}