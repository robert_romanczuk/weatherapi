package com.service;

import com.config.OpenWeatherMapConfig;
import com.model.WeatherJsonModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class OpenWeatherMapService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OpenWeatherMapService.class);

    public OpenWeatherMapConfig openWeatherMapConfig;

    @Autowired
    public OpenWeatherMapService(OpenWeatherMapConfig openWeatherMapConfig) {
        this.openWeatherMapConfig = openWeatherMapConfig;
    }

    private String createUrl(String innerUrlPart) {
        return String.format("%s%s&APPID=%s", openWeatherMapConfig.getUrl(), innerUrlPart, openWeatherMapConfig.getApiKey());
    }

    public WeatherJsonModel getCityWeather(String cityName) {
        RestTemplate restTemplate = new RestTemplate();

        try {
            return restTemplate.getForObject(createUrl(String.format("weather?q=%s", cityName)), WeatherJsonModel.class);
        } catch (HttpClientErrorException e) {
            LOGGER.error(String.format("%s city not found or bad city name provided.", cityName));
        }

        return null;
    }
}
