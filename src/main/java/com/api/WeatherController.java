package com.api;

import com.model.WeatherJsonModel;
import com.service.OpenWeatherMapService;
import com.service.SqliteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

@RestController
public class WeatherController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherController.class);

    public SqliteService sqliteService;
    public OpenWeatherMapService openWeatherMapService;

    @Autowired
    public WeatherController(SqliteService sqliteService, OpenWeatherMapService openWeatherMapService) {
        this.sqliteService = sqliteService;
        this.openWeatherMapService = openWeatherMapService;
    }

    @RequestMapping("/city-weather/{cityName}")
    public
    @ResponseBody
    Object cityWeather(@PathVariable String cityName) {
        List<Map<String, Object>> cityWeather = sqliteService.getCityWeather(cityName);

        boolean isOlderThanHour = false;
        if (!cityWeather.isEmpty()) {
            LocalDateTime updatedAtTime = LocalDateTime.parse((String) cityWeather.get(0).get("updated_at"), DateTimeFormatter.ofPattern("y-M-d H:m:s"));
            isOlderThanHour = Math.abs(ChronoUnit.HOURS.between(updatedAtTime, LocalDateTime.now(ZoneId.of("GMT")))) >= 1;
        }

        if (cityWeather.isEmpty() || isOlderThanHour) {
            LOGGER.info(String.format("No data for %s. Loading data from weather provider.", cityName));
            WeatherJsonModel cityWeatherData = openWeatherMapService.getCityWeather(cityName);

            if (cityWeatherData != null) {
                sqliteService.saveCityWeather(cityName, cityWeatherData);
                cityWeather = sqliteService.getCityWeather(cityName);
            } else {
                JsonResponse response = new JsonResponse();
                response.setStatus("Error");
                response.setDescription("City data not found! Check provided city name.");
                return response;
            }
        }

        return cityWeather;
    }

    public class JsonResponse {

        private String status;
        private String description;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

    }
}
